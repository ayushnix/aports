# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer:
pkgname=py3-fonttools
_pkgname=fonttools
pkgver=4.42.0
pkgrel=0
pkgdesc="Converts OpenType and TrueType fonts to and from XML"
url="https://github.com/fonttools/fonttools"
arch="all"
license="MIT AND OFL-1.1"
makedepends="
	cython
	py3-gpep517
	py3-installer
	py3-setuptools
	py3-wheel
	python3-dev
	"
checkdepends="py3-fs py3-pytest py3-pytest-xdist py3-brotli"
subpackages="$pkgname-doc $pkgname-pyc"
source="$_pkgname-$pkgver.tar.gz::https://github.com/fonttools/fonttools/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-fonttools" # Backwards compatibility
provides="py-fonttools=$pkgver-r$pkgrel" # Backwards compatibility

prepare() {
	default_prepare
	# remove interpreter line
	sed -i '1d' Lib/fontTools/mtiLib/__init__.py
}

build() {
	CFLAGS="$CFLAGS -O2 -flto=auto" \
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	PATH="$PATH:$PWD" \
		.testenv/bin/python3 -m pytest -n auto
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/fonttools-$pkgver-*.whl
}

doc() {
	replaces="py-$_pkgname-doc" # Backwards compatibility
	provides="py-$_pkgname-doc=$pkgver-r$pkgrel" # Backwards compatibility
	default_doc
}

sha512sums="
c5b6963d420dd8bf2bae776880bf23c9e91aeb04f62fb3231cfb0af00008b82a19814138311c2b06cc8df1dbb83c0cd1fc1312a540918a3727407cee81d19116  fonttools-4.42.0.tar.gz
"
