# Contributor: prspkt <prspkt@protonmail.com>
# Maintainer: prspkt <prspkt@protonmail.com>
pkgname=py3-zipp
pkgver=3.16.2
pkgrel=0
pkgdesc="Pathlib-compatible object wrapper for zip files"
url="https://github.com/jaraco/zipp"
arch="noarch"
license="MIT"
depends="python3"
makedepends="py3-gpep517 py3-setuptools py3-setuptools_scm py3-wheel"
checkdepends="py3-pytest py3-jaraco.itertools py3-func-timeout py3-jaraco.functools"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/z/zipp/zipp-$pkgver.tar.gz"
builddir="$srcdir/zipp-$pkgver"

replaces="py-zipp" # Backwards compatibility
provides="py-zipp=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/zipp*.whl
}

sha512sums="
cb7f19638b2bc2bb1c38241078836c4e484b8f1fae8232073da25967f01e449eb8a6c870d2afde285ea9ae319785a9e4b7cb0e5edc2d0f4690f3ce27c359be93  zipp-3.16.2.tar.gz
"
