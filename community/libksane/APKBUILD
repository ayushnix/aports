# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=libksane
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/graphics/"
pkgdesc="An image scanning library"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="
	ki18n-dev
	ksanecore-dev
	ktextwidgets-dev
	kwallet-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
_repo_url="https://invent.kde.org/graphics/libksane.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/libksane-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7682e5cbfc5afea90ea58d81b6b9fc36728c962b0537df1ff4a72a25da1923d36e1eb7b6a66233eb65607d3515559231832e645668c6f9b2ba2ba5eeb9b90723  libksane-23.04.3.tar.xz
"
