# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=xournalpp
pkgver=1.2.0
pkgrel=4
pkgdesc="Xournal++ is a handwriting notetaking software with PDF annotation support"
url="https://github.com/xournalpp/xournalpp"
arch="all !s390x !riscv64"
license="GPL-2.0-or-later"
depends="adwaita-icon-theme"
makedepends="
	cmake
	glib-dev
	gtk+3.0-dev
	gtksourceview4-dev
	librsvg-dev
	libsndfile-dev
	libx11-dev
	libxi-dev
	libxml2-dev
	libzip-dev
	lsb-release
	lua5.4-dev
	poppler-dev
	portaudio-dev
	samurai
	"
options="!check" # no tests
subpackages="$pkgname-dbg $pkgname-lang"
source="xournalpp-$pkgver.tar.gz::https://github.com/xournalpp/xournalpp/archive/v$pkgver.tar.gz
	no-execinfo.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi

	# Increase stack-size to avoid crashes when using pen for input
	LDFLAGS="$LDFLAGS -Wl,-z,stack-size=2097152" \
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_SHARED_LIBS=True \
		-DLUA_LIBRARIES="/usr/lib/lua5.4/liblua.so" \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		$CMAKE_CROSSOPTS
	cmake --build build --target all translations
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9b8a1d0cb3502ea81008d00310731b10946608e5d73ddb4c785e30012389d788ccf1958e9e36875937b9c1475f562275a4e301fd641b3e9eb72c10466804d42b  xournalpp-1.2.0.tar.gz
0fc15900d9f3750fab742dc9bab88a79a1b746a610752767b04b796b0f23f5f9e3baff875abf580d9255e26e6d2fa9db922ba37cd08df6872df6f012d5252cca  no-execinfo.patch"
