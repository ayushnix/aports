# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=audiocd-kio
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/multimedia/"
pkgdesc="Kioslave for accessing audio CDs"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	cdparanoia-dev
	extra-cmake-modules
	flac-dev
	kcmutils-dev
	kconfig-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	libkcddb-dev
	libkcompactdisc-dev
	libvorbis-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/multimedia/audiocd-kio.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/audiocd-kio-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a10315c4ec5a529421adbab64a3f8fe02cf0e13bb5b501a4b57bd2d58522be0118d80fef8853d8dc178e75e04572b584f487aafb6f991068149159dbb6391ab7  audiocd-kio-23.04.3.tar.xz
"
