# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kcachegrind
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/development/org.kde.kcachegrind"
pkgdesc="A profile data visualization tool, used to determine the most time consuming parts in the execution of a program"
license="GPL-2.0-only AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	karchive-dev
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt5-qtbase-dev
	qt5-qttools-dev
	samurai
	"
_repo_url="https://invent.kde.org/sdk/kcachegrind.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kcachegrind-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
191edbcc5ff6f10d4f5668b0c3cdfef97aca440ad79c0e0c0fb8e34a80dff1b2eac4662070ae5f445eaf20836dc74f1ed877284b645fa0383cd5b95ebcbef5c7  kcachegrind-23.04.3.tar.xz
"
