# Contributor: Jonah Brüchert <jbb@kaidan.im>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=angelfish
pkgver=23.04.3
pkgrel=1
pkgdesc="Small Webbrowser for Plasma Mobile"
# armhf blocked by extra-cmake-modules
# ppc64le and s390x blocked by qt5-qtwebengine
# riscv64 disabled due to missing rust in recursive dependency
arch="all !ppc64le !s390x !armhf !riscv64"
url="https://phabricator.kde.org/source/plasma-angelfish/"
license="GPL-3.0-or-later"
depends="
	kirigami-addons
	kirigami2
	plasma-framework
	purpose
	qt5-qtbase-sqlite
	qt5-qtfeedback
	qt5-qtquickcontrols2
	"
makedepends="
	corrosion
	extra-cmake-modules
	kdeclarative-dev
	ki18n-dev
	kio-dev
	kirigami-addons-dev
	kirigami2-dev
	plasma-framework-dev
	purpose-dev
	qqc2-desktop-style-dev
	qt5-qtfeedback-dev
	qt5-qtwebengine-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/network/angelfish.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/angelfish-$pkgver.tar.xz"
options="net" # net required to download Rust dependencies

provides="plasma-angelfish=$pkgver-r$pkgrel" # Backwards compatibility
replaces="plasma-angelfish" # Backwards compatibility

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 $_repo_url .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure -j1
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1f9098c331be21a71d51d5e89aa13df593f732a8b1cff2697b49154b6367cc099d0a1f117b75efa884459769a72f6de5a0066b5286ef5bde56a8a89b0fd87641  angelfish-23.04.3.tar.xz
"
