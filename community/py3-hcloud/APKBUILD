# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=py3-hcloud
pkgver=1.27.2
pkgrel=0
pkgdesc="Official Hetzner Cloud Python library"
url="https://github.com/hetznercloud/hcloud-python"
license="MIT"
arch="noarch"
depends="py3-dateutil py3-requests"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://github.com/hetznercloud/hcloud-python/archive/v$pkgver/py3-hcloud-$pkgver.tar.gz"
builddir="$srcdir/hcloud-python-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/hcloud-$pkgver-py3-none-any.whl
}

sha512sums="
8e5a205aed572be042df9bbe1bfb188112bc1b08367875f6da1d258e27686865a6e4d71e83697ad97ead4cb8879d4655363fa74b6f94d4b10ed21b2db1e7eda5  py3-hcloud-1.27.2.tar.gz
"
