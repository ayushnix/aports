# Contributor: kohnish <kohnish@gmx.com>
# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=buildah
pkgver=1.31.2
pkgrel=0
pkgdesc="tool that facilitates building OCI container images"
url="https://github.com/containers/buildah"
license="Apache-2.0"
arch="all"
depends="oci-runtime shadow-subids slirp4netns containers-common"
makedepends="go go-md2man lvm2-dev gpgme-dev libseccomp-dev btrfs-progs-dev bash"
subpackages="$pkgname-doc"
options="!check" # tests require root privileges
source="https://github.com/containers/buildah/archive/v$pkgver/buildah-$pkgver.tar.gz"

# secfixes:
#   1.28.0-r0:
#     - CVE-2022-2990
#   1.21.3-r0:
#     - CVE-2021-3602
#   1.19.4-r0:
#     - CVE-2021-20206
#   1.14.4-r0:
#     - CVE-2020-10696

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	GIT_COMMIT="$pkgver" make
}

package() {
	GIT_COMMIT="$pkgver" make install PREFIX=/usr DESTDIR="$pkgdir"
}

sha512sums="
c8611016448de7e9548ff692d1b352d7234f357440001106276e513df135a28ba0d4413a14f5c30e4b88615267d92f4aea63e1687b84a0638f1f9ab4f348c74a  buildah-1.31.2.tar.gz
"
