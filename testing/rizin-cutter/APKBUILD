# Contributor: Leon Marz <main@lmarz.org>
# Maintainer: Leon Marz <main@lmarz.org>
pkgname=rizin-cutter
pkgver=2.3.0
pkgrel=0
pkgdesc="Reverse Engineering Platform powered by rizin"
url="https://cutter.re"
arch="all !armhf" # syntax-highlighting not available
license="GPL-3.0-only"
depends="rizin"
makedepends="
	cmake
	graphviz-dev
	python3-dev
	qt5-qtsvg-dev
	qt5-qttools-dev
	rizin-dev
	samurai
	syntax-highlighting-dev
	"
subpackages="$pkgname-dev"
_translations_commit=974298653ba71b958e1b6c83f6011f5fefff6236
source="https://github.com/rizinorg/cutter/releases/download/v$pkgver/Cutter-v$pkgver-src.tar.gz"
builddir="$srcdir/Cutter-v$pkgver"
options="!check" # upstream does not provide a testsuite

build() {
	CXXFLAGS="$CXXFLAGS -O2 -flto=auto" \
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=None \
		-DCUTTER_USE_BUNDLED_RIZIN=OFF \
		-DCUTTER_USE_ADDITIONAL_RIZIN_PATHS=OFF \
		-DCUTTER_ENABLE_GRAPHVIZ=ON \
		-DCUTTER_ENABLE_PYTHON=ON

	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
98e6c7eccd5dbd09d883775a7d7e0c997778caeaa83cc9e4a5b7a9d61ac8d834eda57f6e05ae1b943810823296942cacd714f367dc1b7cfe25f6171522dd25bd  Cutter-v2.3.0-src.tar.gz
"
