# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: Sören Tempel <soeren+alpine@soeren-tempel.net>
pkgname=guile-gnutls
pkgver=3.7.14
_gitlabsha=1fdc941351d54cd7affda1bb912b9ca5
pkgrel=0
pkgdesc="Guile Scheme bindings for GnuTLS"
url="https://gitlab.com/gnutls/guile"
arch="all"
license="LGPL-2.1-or-later"
depends="guile"
makedepends="gnutls-dev guile-dev texinfo"
subpackages="$pkgname-doc"
options="!strip" # see https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=907061
source="https://gitlab.com/gnutls/guile/uploads/$_gitlabsha/guile-gnutls-$pkgver.tar.gz
	tests-do-not-use-hostname-for-sni.patch"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-srp-authentication
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
ff69a7b005b6230bef17cb49563449018f5908ae18ae166bf092f3461ef9d49fe306a301d59c266d550523faa5a20393264e5dcbb734f56aa2c778f3130d89fc  guile-gnutls-3.7.14.tar.gz
f4e5aae91d589b7313ae3e8b31d295535dfb64349ba64c0809aa56f7b2f0b50447a359d7e816ad5c36449646e1ed91841c560e7cf36fe2ed8111160726da01d5  tests-do-not-use-hostname-for-sni.patch
"
